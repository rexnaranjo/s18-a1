# s18-a1

## Add the following users
```
db.users.insertMany([
	{"firstName":"Diane","lastName":"Murphy","email":"dmurphy@mail.com","isAdmin":"no","isActive":"yes"},
	{"firstName":"Mary","lastName":"Patterson","email":"mpatterson@mail.com","isAdmin":"no","isActive":"yes"},
	{"firstName":"Jeff","lastName":"Firrelli","email":"jfirrelli@mail.com","isAdmin":"no","isActive":"yes"},
	{"firstName":"Gerard","lastName":"Bondur","email":"gbondur@mail.com","isAdmin":"no","isActive":"yes"},
	{"firstName":"Pamela","lastName":"Castillo","email":"pcastillo@mail.com","isAdmin":"yes","isActive":"no"},
	{"firstName":"George","lastName":"Vanauf","email":"gvanauf@mail.com","isAdmin":"yes","isActive":"yes"},
]);
```

## Add the following courses
```
db.users.insertMany([
	{"courseName":"Professional Development","price":"10000"},
	{"courseName":"Business Processing","price":"13000"}
]);
```

## Get user IDs and add them as enrollees of the courses (update)
```
db.users.find({"lastName":"Murphy"}, {_id:1});
db.users.find({"lastName":"Firrelli"}, {_id:1});
db.users.find({"lastName":"Bondur"}, {_id:1});
db.users.find({"lastName":"Patterson"}, {_id:1});
```

```
db.users.updateOne({
	"courseName":"Professional Development"
}, {
	$set: {
		"enrollees": ["Murphy", "Firrelli"]
	}
});

db.users.updateOne({
	"courseName":"Business Processing"
}, {
	$set: {
		"enrollees": ["Bondur", "Patterson"]
	}
});
```